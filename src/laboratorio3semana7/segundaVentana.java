/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio3semana7;

import static java.awt.SystemColor.text;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import java.util.TimerTask;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SpinnerNumberModel;
import javax.swing.Timer;

/**
 *
 * @author usuario
 */
public class segundaVentana extends javax.swing.JFrame {

    int contador = 30;

    //En esta parte voy a hacer lo del timer.
    //En esta parte voy a configurar los spinnners.
    //Thread cronometro = new Thread();
    //En esta parte obento mis numeros random.
    Random randon = new Random();
    /*
        En esta ventana, se debe adivinar una
secuencia de tres dígitos de un código
random que se generó; cada dígito a lo
interno se debe generar por separado porque el rango debe ser de 1 a 3.
     */
    int primerNumeroRan = randon.nextInt(2) + 1;//Le sumo uno porque los random comienzan desde 0.
    int segundoNumeroRan = randon.nextInt(2) + 1;
    int tercerNumeroRan = randon.nextInt(2) + 1;

    public Timer tiempo;//Se crea el objeto.

    /**
     * Creates new form SegundaVetana
     */
    public segundaVentana() {
        initComponents();

        setLocationRelativeTo(null);//Centrar pantalla.
        timer.start();

    }
    //En esta parte voy a decir que quiero que haga el cronómetro.
    Timer timer = new Timer(1000, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            lblCronometro.setText(Integer.toString(contador));
            contador--;
            if (contador == 0) {
                dispose();

            }

        }

    });

//mil milisegundos equivalen a un segundo.
    //En esta parte voy a obtener los numeros randoms.
    public void adivinarCombinacionSpinners() {

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnSalir = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jspnnrPrimero = new javax.swing.JSpinner();
        jspnnrSegundo = new javax.swing.JSpinner();
        jspnnrTercero = new javax.swing.JSpinner();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        btnTerceraVent = new javax.swing.JButton();
        lblTiempo = new javax.swing.JLabel();
        lblMensaje = new javax.swing.JLabel();
        lblCronometro = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel1.setText("Debe adivinar tres números que han sido tomados de manera aleatoria");

        jspnnrPrimero.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jspnnrPrimero.setModel(new javax.swing.SpinnerNumberModel(1, 1, 3, 1));
        jspnnrPrimero.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jspnnrPrimeroStateChanged(evt);
            }
        });

        jspnnrSegundo.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jspnnrSegundo.setModel(new javax.swing.SpinnerNumberModel(1, 1, 3, 1));
        jspnnrSegundo.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jspnnrSegundoStateChanged(evt);
            }
        });

        jspnnrTercero.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jspnnrTercero.setModel(new javax.swing.SpinnerNumberModel(1, 1, 3, 1));
        jspnnrTercero.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jspnnrTerceroStateChanged(evt);
            }
        });

        jLabel2.setText("Primer número a adivinar");

        jLabel3.setText("Segundo número a adivinar");

        jLabel4.setText("Tercer número a adivinar");

        btnTerceraVent.setText("Ir a la tercera ventana");
        btnTerceraVent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTerceraVentActionPerformed(evt);
            }
        });

        lblTiempo.setText("Tiempo transcurrido:");

        lblMensaje.setText("Tiempo máximo 30 segundos.");

        lblCronometro.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        lblCronometro.setText("0:0:0");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 667, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 6, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnSalir)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnTerceraVent)
                                .addGap(137, 137, 137))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblMensaje)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(lblTiempo)
                                        .addGap(33, 33, 33)
                                        .addComponent(lblCronometro)))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(102, 102, 102)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jspnnrPrimero, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jspnnrSegundo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jspnnrTercero, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(216, 216, 216))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(jLabel1)
                .addGap(76, 76, 76)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jspnnrPrimero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jspnnrSegundo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jspnnrTercero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 139, Short.MAX_VALUE)
                .addComponent(lblMensaje)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTiempo)
                    .addComponent(lblCronometro))
                .addGap(89, 89, 89)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSalir)
                    .addComponent(btnTerceraVent))
                .addGap(24, 24, 24))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed

        dispose();


    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnTerceraVentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTerceraVentActionPerformed
        final JPanel panel = new JPanel();//Esto lo tuve que agregar para el mensage de warning
        int spin1 = (int) jspnnrPrimero.getValue();
        int spin2 = (int) jspnnrSegundo.getValue();
        int spin3 = (int) jspnnrTercero.getValue();

        if (spin1 == primerNumeroRan && spin2 == segundoNumeroRan && spin3 == tercerNumeroRan) {

            terceraVentana llamarTerceraVentana = new terceraVentana();
            llamarTerceraVentana.setVisible(true);

        } else {
            JOptionPane.showMessageDialog(panel, "Números incorrectos", "Warning", JOptionPane.WARNING_MESSAGE);

        }


    }//GEN-LAST:event_btnTerceraVentActionPerformed

    private void jspnnrPrimeroStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jspnnrPrimeroStateChanged
        //En esta parte lo que hago es inabilitar el spiner, cuando se selecciona el correcto, no permmite seguir eligiendo en ese spiner.
        if ((int) jspnnrPrimero.getValue() == primerNumeroRan) {
            jspnnrPrimero.setEnabled(false);

        }


    }//GEN-LAST:event_jspnnrPrimeroStateChanged

    private void jspnnrSegundoStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jspnnrSegundoStateChanged
        //En esta parte lo que hago es inabilitar el spiner, cuando se selecciona el correcto, no permmite seguir eligiendo en ese spiner.

        if ((int) jspnnrSegundo.getValue() == segundoNumeroRan) {
            jspnnrSegundo.setEnabled(false);

        }
    }//GEN-LAST:event_jspnnrSegundoStateChanged

    private void jspnnrTerceroStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jspnnrTerceroStateChanged
        //En esta parte lo que hago es inabilitar el spiner, cuando se selecciona el correcto, no permmite seguir eligiendo en ese spiner.

        if ((int) jspnnrTercero.getValue() == tercerNumeroRan) {
            jspnnrTercero.setEnabled(false);

        }


    }//GEN-LAST:event_jspnnrTerceroStateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(segundaVentana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(segundaVentana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(segundaVentana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(segundaVentana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new segundaVentana().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSalir;
    private javax.swing.JButton btnTerceraVent;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JSpinner jspnnrPrimero;
    private javax.swing.JSpinner jspnnrSegundo;
    private javax.swing.JSpinner jspnnrTercero;
    private javax.swing.JLabel lblCronometro;
    private javax.swing.JLabel lblMensaje;
    private javax.swing.JLabel lblTiempo;
    // End of variables declaration//GEN-END:variables
}
